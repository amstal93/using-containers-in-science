---
title: "Using Containers in HPC with Singularity"
teaching: 10
exercises: 20
questions:
- "What is the difference between Docker and Singularity?"
- "Why is Singularity more suitable for HPC?"
objectives:
- "Understand the main differences between Singularity and Docker."
- "Learn the basic commands of Singularity."
keypoints:
- "Use the `singularity run` command to run a Singularity image."
- "Define the construction manual of a Singularity image in a definitions file."
- "Singularity is more often available in HPC systems."
---

{% include links.md %}

## Quick Start
After having successfully installed Singularity on your system the initial step
is to run your first container.

~~~
$ singularity run library://sylabsed/examples/lolcow
~~~
{: .language-terminal}

The leading `library://` part in the image name tells Singularity to look for
the image in the [Singularity Cloud Library](https://cloud.sylabs.io/).
This is the equivalent to [Docker Hub](https://hub.docker.com/) in the
Singularity world.
The output will look something like this:

~~~
INFO:    Downloading library image
79.9MiB / 79.9MiB [======================================================================================================================================================] 100 % 11.6 MiB/s 0s
 _________________________________________
/ You are scrupulously honest, frank, and \
| straightforward. Therefore you have few |
\ friends.                                /
 -----------------------------------------
        \   ^__^
         \  (oo)\_______
            (__)\       )\/\
                ||----w |
                ||     ||
~~~
{: .output}

Let's try another example by running an Ubuntu image using Singularity.
Therefore, let us use the
[`library/default/ubuntu:20.04`](https://cloud.sylabs.io/library/library/default/ubuntu)
image from the [Singularity Cloud Library](https://cloud.sylabs.io/).

~~~
$ singularity run library://library/default/ubuntu:20.04
~~~
{: .language-terminal}

Using this command we are presented a shell inside the Ubuntu Singularity
container.
Once inside the container, you are the same user as you are on the host system.

~~~
Singularity> whoami
maria
~~~
{: .language-terminal}

Singularity automatically makes your home directory (`/home/$USER`) available
in the container.
Unlike with Docker, it is not necessary to explicitly mount the directories.
They are made available by default.

With Singularity, you can easily reuse existing Docker images from Docker Hub
and run them as a Singularity container.
Let us run the [`python:3.9`](https://hub.docker.com/_/python) Docker image as
a Singularity container.
Therefore, we `pull` the image first.

~~~
$ singularity pull docker://python:3.9
~~~
{: .language-terminal}

This command downloads the image from Docker Hub (`docker://`) and converts it
into the Singularity specific image format called `SIF file`.
A file called `python_3.9.sif` was created in your current directory.
Run it as shown below.

~~~
$ singularity run python_3.9.sif
Python 3.9.2 (default, Mar 12 2021, 18:54:15) 
[GCC 8.3.0] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>>
~~~
{: .language-terminal}

## Building Containers from Singularity Definition Files

As demonstrated in the Docker build lesson we want to build our first custom
Singularity image as well.
Therefore, we need to create the Singularity definition file, i.e. the
equivalent to `Dockerfile`.
We will recreate the lolcow image used for the first `run` command in this episode.
Therefore, we create a file called `lolcow.def` using the editor of your choice.
This file contains the construction manual for the Singularity image.

~~~
Bootstrap: docker
From: ubuntu:20.04

%post
  apt-get -qy update
  apt-get -qy install fortune cowsay lolcat

%environment
  export LC_ALL=C
  export PATH=/usr/games:$PATH

%runscript
  fortune | cowsay | lolcat
~~~
{: .language-singularity}

Build the image using the `singularity build` command.

~~~
$ sudo singularity build lolcow.sif lolcow.def
~~~
{: .language-terminal}

**Note:** The Singularity build command requires root privileges.

This command creates the file `lolcow.sif`.
It is run using the `singularity run` command.

~~~
$ sudo singularity run lolcow.sif
~~~
{: .language-terminal}

~~~
 ________________________________________
/ Good day for overcoming obstacles. Try \
\ a steeplechase.                        /
 ----------------------------------------
        \   ^__^
         \  (oo)\_______
            (__)\       )\/\
                ||----w |
                ||     ||
~~~
{: .output}

Let's take apart the Singularity definition file.

### Header

Each Singularity definition file needs to start with the header part consisting
of at least the `Bootstrap` keyword.
In our example we use the `docker` bootstrap agent telling Singularity to
get the image from a Docker registry.
This agent needs to be combined with the `From` keyword to let Singularity know,
which base image to use.

~~~
Bootstrap: docker
From: ubuntu:20.04
~~~
{: .language-singularity}

> ## Use Images From Other Container Registries
> If you want to use another registry, e.g. the GitLab container registry,
> it is easily possible. Specify the full name of the image according to the GitLab
> container registry naming convention as described in the
> [documentation](https://docs.gitlab.com/ee/user/packages/container_registry/#image-naming-convention).
>
> Generic example:
>
> ~~~
> Bootstrap: docker
> From: <registry URL>/<namespace>/<project>/<image>
> ~~~
> {: .language-singularity}
{: .callout}

A list of preferred bootstrap agents is available
[here](https://sylabs.io/guides/3.7/user-guide/definition_files.html#preferred-bootstrap-agents).

### Sections

The main content of the definition file is broken into sections.
In our example we used three different sections:

* `%post`
    * In this section you can download files from the internet with tools like
      `git`, `wget` or `pip`,
    * You can install new software and libraries,
    * Create configuration files,
    * Create files and directories, etc.
* `%environment`
    * This section allows you to define environment variables which are set at
      runtime.
    * These variables are not set at build time,
      when running the `singularity build` command.
* `%runscript`
    * The commands specified in this section are executed when the container
      image is run. (`singularity run`)

Please refer to the [official documentation](https://sylabs.io/guides/3.7/user-guide/definition_files.html#sections)
for a complete list of available sections and their usage.

## Singularity vs Docker in a Nutshell

|                                       | Docker                                                                                               | Singularity                                                                |
|---------------------------------------|------------------------------------------------------------------------------------------------------|----------------------------------------------------------------------------|
| Isolation from host                   | Shares little by default.                                                                            | Isolation not the primary focus. By default shares most everything.        |
| Supported Host Operating Systems (OS) | Windows, Mac, Linux                                                                                  | Linux                                                                      |
| Security                              | Users running docker commands need to be in a special `docker` group to gain elevated system access. | Users can run Singularity containers without special privileges.           |
| Data Persistence                      | No host filesystem available by default.                                                             | Writable bind-mounts of the user home directory are automatically created. |
| Primary target group                  | Developers, DevOps                                                                                   | Scientific Application Users/Developers                                    |
| HPC                                   | Not suitable for HPC: requires elevated permissions                                                  | Integrates well with MPI, GPUs, Infiniband and Schedulers (e.g. SLURM)     |
| Ecosystem                             | Larger ecosystem; more sophisticated registries and preconfigured images                             | Smaller ecosystem; is able to use Docker images.                           |
