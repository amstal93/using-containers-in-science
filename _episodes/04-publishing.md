---
title: "Publishing Docker Images to a Container Registry"
teaching: 15
exercises: 0
questions:
- "How to publish Docker images on the GitLab Container Registry?"
- "How to assign version tags to your Docker images?"
- "How to delete images or tags?"
objectives:
- "Get to grips with pushing Docker images."
- "Assign tags to your Docker images."
- "Delete images or tags created."
keypoints:
- "Pushing Docker images."
- "Tagging Docker images."
- "Deleting Docker images or tags."
---

{% include links.md %}

## Publish Docker Images

In the previous episode we learned how to build our own Docker images.
In this episode we are dealing with how to publish our Docker images in a container registry like the GitLab container registry or Docker Hub.

For later reference, here is the respective [GitLab documentation](https://docs.gitlab.com/ee/user/packages/container_registry/) and [Docker documentation](https://docs.docker.com/docker-hub/).

This is how a GitLab Container Registry page looks like. It displays the most useful commands initially.

![GitLab Container Registry commands](../fig/gitlab-container-registry-commands.png){: width="800px" height="491px"}

### Publishing on GitLab Container Registry

Publishing to a GitLab container registry involves five steps:

1. Create a GitLab user.
2. Create a GitLab repository.
3. Create a tag for your Docker image.
4. Log in into the GitLab container registry with an access token.
5. Push your image to the container registry.

#### Create a GitLab User

In order to publish a Docker image to the GitLab Container Registry you need to [sign up](https://gitlab.com/users/sign_up) for this service.

#### Create a GitLab Repository

Once you signed up for your preferred GitLab service you can create your own [GitLab repository](https://docs.gitlab.com/ee/user/project/repository/).
By default the GitLab container registry is enabled for a new GitLab project.

#### Create a Tag for your Docker Image

Docker image tags are alias names for rather cryptic Docker image IDs and specify the respective container registry to which the Docker image will be published along with a version string of that Docker image:

~~~
$ docker image list
REPOSITORY                                                            TAG            IMAGE ID            CREATED           SIZE
my-ipython-image                                                      latest         c42929207891        1 hour ago        991MB

$ docker image tag <image name>:<tag> <GitLab Container Registry URL>/<GitLab username>/<GitLab repository name>:<tag>
$ docker image tag my-ipython-image:latest registry.gitlab.com/christian.hueser.hzdr/my-ipython-image-project:latest

$ docker image list
REPOSITORY                                                            TAG            IMAGE ID            CREATED           SIZE
my-ipython-image                                                      latest         c42929207891        1 hour ago        991MB
registry.gitlab.com/christian.hueser.hzdr/my-ipython-image-project    latest         c42929207891        1 hour ago        991MB
~~~
{: .language-terminal}

#### Log in into the GitLab container registry with an access token

Communication with a GitLab container registry is only possible if you [log in](https://docs.docker.com/engine/reference/commandline/login/) to the GitLab container registry on the command line interface beforehand.

~~~
$ docker login registry.gitlab.com --username <GitLab username>
Password:
WARNING! Your password will be stored unencrypted in /home/<linux os user>/.docker/config.json.
Configure a credential helper to remove this warning. See
https://docs.docker.com/engine/reference/commandline/login/#credentials-store

Login Succeeded
~~~
{: .language-terminal}

A password prompt will be displayed and the password will be put into `~/.docker/config.json`.

> **Note:** The password will be stored in a base64-encoding which is not an encryption.
> Never re-use passwords for services!

Alternatively, you might want to [create an access token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html) with scope `api` or `read_registry` and `write_registry` and use that one instead of your password.

#### Push your Image to the container registry

The final step is now to publish your Docker image to the GitLab container registry of your repository:

~~~
$ docker push registry.gitlab.com/<GitLab username>/<GitLab repository name>:<tag>
$ docker push registry.gitlab.com/christian.hueser.hzdr/my-ipython-image-project:latest
The push refers to repository [registry.gitlab.com/christian.hueser.hzdr/my-ipython-image-project]
c86a58ac7492: Pushed
0804a4895ec9: Pushed
f92738a52ffe: Pushed
cce98789e54a: Pushed
e3d73f29c674: Pushed
10bf86ff9f6a: Pushed
da654bc8bc80: Pushed
4ef81dc52d99: Pushed
909e93c71745: Pushed
7f03bfe4d6dc: Pushed
latest: digest: sha256:876873ac311ac8d5038ae08122aa345e9ec6951c6f2742f2e7f459cb4be2e75e size: 2429
~~~
{: .language-terminal}

On the GitLab side you are now able to see the published Docker images in the GitLab container registry:

![GitLab Container Registry list](../fig/gitlab-container-registry-list.png){: width="800px" height="491px"}

The detailed list of all tags in a GitLab container registry can also be obtained:

![GitLab Container Registry tags](../fig/gitlab-container-registry-tags.png){: width="800px" height="489px"}

> ### Try it yourself: Publishing on Docker Hub
>
> Publishing Docker images to a container registry like Docker Hub involves five steps which are quite similar to those five steps when publishing to a GitLab container registry:
>
> 1. Create a [Docker ID](https://hub.docker.com/signup).
> 2. Create a [Docker repository](https://docs.docker.com/docker-hub/repos/).
> 3. Create a tag for your Docker image.
> 4. Log in into Docker Hub with an [access tokens](https://docs.docker.com/docker-hub/access-tokens/).
> 5. Push your image to the container registry.
>
> Are you able to reproduce these steps given the five steps for publishing Docker images to a GitLab container registry?
{: .challenge}

### Understanding Docker Image Tags

Images can have several tags, i.e. alias names for the respective image ID. So far we were labeling the newest image with `latest`.

It is good practice to label images with semantic version strings `[major].[minor].[patch]` like `1.2.3` according to their release version.
See https://semver.org/ for a full specification how semantic versioning works.

Depending on the container registry used your images need to be tagged differently:

~~~
$ docker image list
REPOSITORY                                                            TAG            IMAGE ID            CREATED           SIZE
my-ipython-image                                                      latest         c42929207891        1 hour ago        991MB
registry.gitlab.com/christian.hueser.hzdr/my-ipython-image-project    latest         c42929207891        1 hour ago        991MB

$ docker image tag <image name>:<tag> <GitLab Container Registry URL>/<GitLab username>/<GitLab repository name>:<semantic version tag>
$ docker image tag my-ipython-image:latest registry.gitlab.com/christian.hueser.hzdr/my-ipython-image-project:0.1.0

$ docker image list
REPOSITORY                                                             TAG            IMAGE ID            CREATED           SIZE
my-ipython-image                                                       latest         c42929207891        1 hour ago        991MB
registry.gitlab.com/christian.hueser.hzdr/my-ipython-image-project     0.1.0          c42929207891        1 hour ago        991MB
registry.gitlab.com/christian.hueser.hzdr/my-ipython-image-project     latest         c42929207891        1 hour ago        991MB
~~~
{: .language-terminal}

As you can see a tag may include the following information:

* URL of the container registry, defaults to `docker.io`.
* The username of the user of the container registry service.
* The name of the repository in the container registry.
* The tag specifying the version of the image to be added to the container registry, defaults to `latest`.

> **Note:** The important aspect to emphasize here is that tags may refer to the same image as indicated by the same image ID.

### Delete Docker Images or Tags from GitLab Container Registry

**Note: The remote images are irreversibly gone after deletion if they are not also stored locally.**

Log in into GitLab, navigate to the tag you would like to delete and click the trash icon.

![GitLab Container Registry delete](../fig/gitlab-container-registry-delete.png){: width="800px" height="489px"}

### Remove Docker Images or Tags Locally

**Note: The local images are irreversibly gone after deletion if they were not pushed to a remote registry earlier.**

Tags can be deleted by their tag name:

~~~
$ docker image list
REPOSITORY                                                             TAG            IMAGE ID            CREATED           SIZE
my-ipython-image                                                       latest         c42929207891        1 hour ago        991MB
registry.gitlab.com/christian.hueser.hzdr/my-ipython-image-project     0.1.0          c42929207891        1 hour ago        991MB
registry.gitlab.com/christian.hueser.hzdr/my-ipython-image-project     latest         c42929207891        1 hour ago        991MB

$ docker image rm <GitLab Container Registry URL>/<GitLab username>/<GitLab repository name>:<tag>
$ docker image rm registry.gitlab.com/christian.hueser.hzdr/my-ipython-image-project:0.1.0
Untagged: registry.gitlab.com/christian.hueser.hzdr/my-ipython-image-project:0.1.0

$ docker image list
REPOSITORY                                                             TAG            IMAGE ID            CREATED           SIZE
my-ipython-image                                                       latest         c42929207891        1 hour ago        991MB
registry.gitlab.com/christian.hueser.hzdr/my-ipython-image-project     latest         c42929207891        1 hour ago        991MB
~~~
{: .language-terminal}

Images can be deleted by their image ID:

~~~
$ docker image list
my-ipython-image                                                       latest         c42929207891        1 hour ago        991MB
registry.gitlab.com/christian.hueser.hzdr/my-ipython-image-project     latest         c42929207891        1 hour ago        991MB

$ docker image rm <image ID>
$ docker image rm c42929207891
~~~
{: .language-terminal}

Also, all unused images can be deleted with the `prune` sub-command:

~~~
$ docker image prune
~~~
{: .language-terminal}
