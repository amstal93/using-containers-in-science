---
title: Setup
---

## Docker installation

If you want to install Docker on your local system, please follow the installation guide for your
Operating System below.

### Linux

Please refer to the
[Installation Guide for Linux](https://docs.docker.com/install/linux/docker-ce/ubuntu/)
for up-to-date installation instructions.

### Mac OS

Please refer to the
[Installation Guide for MacOS](https://docs.docker.com/docker-for-mac/install/) for up-to-date
installation instructions.

### Windows

Please refer to the
[Installation Guide for Windows](https://docs.docker.com/docker-for-windows/install/) for
up-to-date installation instructions.

### Verify your Installation

If you successfully install Docker on your system, you can test your installation by running the
following:

~~~
$ docker run hello-world
Hello from Docker!
This message shows that your installation appears to be working correctly.
~~~
{: .language-terminal}

## Singularity Installation

Singularity is currently available for Linux-based OS. Please read the
[Installation Instructions](https://www.sylabs.io/guides/latest/user-guide/quick_start.html#quick-installation-steps)

{% include links.md %}
